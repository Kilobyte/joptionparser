package de.kilobyte22.optionparse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class OptionParser {

    private Map<String, Option> longOptions = new HashMap<String, Option>();
    private Map<Character, Option> shortOptions = new HashMap<Character, Option>();

    /**
     * Adds a new option
     * @param name the primary name for the option
     * @return a reference to the option
     */
    public Option addOption(String name) {
        return new Option(name, this);
    }

    /**
     * Tells the parser to parse the options
     * @param args An array of arguments
     * @return An {@link de.kilobyte22.optionparse.OptionSet} containing the result
     */
    public OptionSet parse(String[] args) {
        OptionSet ret = new OptionSet(this);

        boolean noparse = false;

        Option tmpoption = null;

        for (String arg : args) {
            if (noparse) {
                ret.addNonArg(arg);
            } else {
                if (arg.startsWith("-")) {
                    if (tmpoption != null) {
                        if (tmpoption.getArgumentType() == Option.ArgumentType.NEEDED)
                            throw new OptionParseException("option --" + tmpoption.getName() + " needs an argument");
                        else {
                            ret.addOption(tmpoption, tmpoption.getDefaultValue());
                            tmpoption = null;
                        }
                    }
                    if (arg.startsWith("--")) {
                        if (arg.equals("--")) {
                            // seperator
                            noparse = true;
                        } else {
                            String name = arg.substring(2);
                            Option option = longOptions.get(name);
                            if (option == null) {
                                throw new OptionParseException("unknown option --" + name);
                            }
                            if (ret.isSet(option) && !option.allowsMultiple()) {
                                throw new OptionParseException("option --" + name + " may only be used once");
                            }
                            if (option.getArgumentType() != Option.ArgumentType.NONE)
                                tmpoption = option;
                            else
                                ret.addOption(option, null);
                        }
                    } else {
                        if (arg.equals("-")) {
                            // does nothing, code present so that you can have regular arguments directly after arguments with optional param without passing argument
                        } else {
                            String argdata = arg.substring(1);
                            for (int i = 0; i < argdata.length(); i++) {
                                if (tmpoption != null) {
                                    if (tmpoption.getArgumentType() == Option.ArgumentType.NEEDED)
                                        throw new OptionParseException("option --" + tmpoption.getName() + " needs an argument");
                                    else {
                                        ret.addOption(tmpoption, tmpoption.getDefaultValue());
                                        tmpoption = null;
                                    }
                                }
                                char name = argdata.charAt(i);
                                Option option = shortOptions.get(name);
                                if (option == null) {
                                    throw new OptionParseException("unknown option -" + name);
                                }
                                if (ret.isSet(option) && !option.allowsMultiple()) {
                                    throw new OptionParseException("option -" + name + " may only be used once");
                                }
                                if (option.getArgumentType() != Option.ArgumentType.NONE)
                                    tmpoption = option;
                                else
                                    ret.addOption(option, null);
                            }
                        }
                    }
                } else {
                    if (tmpoption != null) {
                        ret.addOption(tmpoption, arg);
                        tmpoption = null;
                    } else {
                        ret.addNonArg(arg);
                    }
                }
            }
        }

        if (tmpoption != null) {
            if (tmpoption.getArgumentType() == Option.ArgumentType.NEEDED)
                throw new OptionParseException("option --" + tmpoption.getName() + " needs an argument");
            else {
                ret.addOption(tmpoption, tmpoption.getDefaultValue());
            }
        }

        return ret;
    }

    void registerOption(String name, Option option) {
        longOptions.put(name, option);
    }

    void registerOption(char name, Option option) {
        shortOptions.put(name, option);
    }

    public static String[] splitArgs(String args) {
        ArrayList<String> ret = new ArrayList<String>();
        String current = "";
        SplitMode mode = SplitMode.SPACE;
        boolean escaped = false, first = true, forceSpace = false;
        for (int i = 0; i < args.length(); i++) {
            char c = args.charAt(i);
            if (forceSpace && c != ' ')
                throw new OptionParseException("char " + i + ": space expected, got " + c);
            if (escaped) {
                current += c;
                escaped = false;
            } else {
                switch (c) {
                    case '\\':
                        escaped = true; break;
                    case '"':
                        if (first) {
                            first = false;
                            mode = SplitMode.SINGLE_QUOTE;
                        } else if (mode == SplitMode.SINGLE_QUOTE) {
                            mode = SplitMode.SPACE;
                            forceSpace = true;
                        } else {
                            current += '"';
                        }
                        break;
                    case '\'':
                        if (first) {
                            first = false;
                            mode = SplitMode.SINGLE_QUOTE;
                        } else if (mode == SplitMode.SINGLE_QUOTE) {
                            mode = SplitMode.SPACE;
                            forceSpace = true;
                        } else {
                            current += "'";
                        }
                        break;
                    case ' ':
                        if (first) break;
                        forceSpace = false;
                        if (mode == SplitMode.SPACE) {
                            ret.add(current);
                            current = "";
                            first = true;
                        } else {
                            current += " ";
                        }
                        break;
                    default:
                        first = false;
                        current += c;
                }
            }
        }
        switch (mode) {
            case SPACE:
                if (current.length() > 0) {
                    ret.add(current);
                }
                break;
            case SINGLE_QUOTE:
                throw new OptionParseException("Unexpected EOL, single quote expected");
            case DOUBLE_QUOTE:
                throw new OptionParseException("Unexpected EOL, double quote expected");
        }
        return ret.toArray(new String[0]);
    }

    private static enum SplitMode {
        SPACE,
        SINGLE_QUOTE,
        DOUBLE_QUOTE
    }
}
