package de.kilobyte22.optionparse;

public class Option {
    private final String name;
    private final OptionParser parser;
    private ArgumentType argumentType = ArgumentType.NONE;
    private boolean multiple = false;
    private String help = null;
    private String defaultValue = null;

    Option(String name, OptionParser parser) {

        this.name = name;
        this.parser = parser;
        parser.registerOption(name, this);
    }

    /**
     * Sets an alias for this Option
     * @param names A list of aliases
     * @return the Option itself, for easy chaining
     */
    public Option alias(String... names) {
        for (String name : names) {
            parser.registerOption(name, this);
        }
        return this;
    }

    /**
     * Adds an alias for this Option
     * @param names A list of aliases
     * @return the Option itself, for easy chaining
     */
    public Option alias(char... names) {
        for (char name : names) {
            parser.registerOption(name, this);
        }
        return this;
    }

    /**
     * Specifies that this option requires an argument
     * @return the Option itself, for easy chaining
     */
    public Option withArgument() {
        argumentType = ArgumentType.NEEDED;
        return this;
    }

    /**
     * Specifies that this Option may have an argument
     * @return the Option itself, for easy chaining
     */
    public Option withOptionalArgument() {
        argumentType = ArgumentType.OPTIONAL;
        return this;
    }

    /**
     * Specifies that this option may occur multiple times
     * @return the Option itself, for easy chaining
     */
    public Option multiple() {
        multiple = true;
        return this;
    }

    /**
     * Sets a help string for this Option
     * @param help The help string to set
     * @return the Option itself, for easy chaining
     */
    public Option help(String help) {
        this.help = help;
        return this;
    }

    /**
     * Sets the default value for this option
     * @param value the default value
     * @return the Option itself, for easy chaining
     */
    public Option defaultValue(String value) {
        defaultValue = value;
        return this;
    }

    /**
     * Checks if this Option may be used multiple times
     * @return {@code true} if this Option may be used multiple times
     */
    public boolean allowsMultiple() {
        return multiple;
    }

    /**
     * Tells of which {@link de.kilobyte22.optionparse.Option.ArgumentType} this Option is
     * @return the {@link de.kilobyte22.optionparse.Option.ArgumentType} this Option is
     */
    public Object getArgumentType() {
        return argumentType;
    }

    /**
     * The original name of this option
     * @return The original name of this option
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the help string of this option
     * @return the help string of this option
     */
    public String getHelp() {
        return help;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    enum ArgumentType {
        /**
         * This Option has no argument
         */
        NONE,
        /**
         * This Option supports an argument, but works without as well
         */
        OPTIONAL,
        /**
         * You must pass an argument when setting this option
         */
        NEEDED
    }
}
