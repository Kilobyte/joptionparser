package de.kilobyte22.optionparse;

public class OptionUsageException extends RuntimeException {
    public OptionUsageException(String message) {
        super(message);
    }
}
