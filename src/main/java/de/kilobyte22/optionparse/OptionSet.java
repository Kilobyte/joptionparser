package de.kilobyte22.optionparse;

import java.util.*;

public class OptionSet {
    private final OptionParser parser;
    private final ArrayList<String> nonoption = new ArrayList<String>();
    private final Map<Option, List<String>> data = new HashMap<Option, List<String>>();

    OptionSet(OptionParser parser) {

        this.parser = parser;
    }

    public void addNonArg(String arg) {
        nonoption.add(arg);
    }

    public boolean isSet(Option option) {
        return data.containsKey(option);
    }

    void addOption(Option tmpoption, String data) {
        List<String> values;
        if (this.data.containsKey(tmpoption)) {
            values = this.data.get(tmpoption);
        } else {
            this.data.put(tmpoption, values = new LinkedList<String>());
        }
        if (data != null)
            values.add(data);
    }

    public String getValueFor(Option option) {
        List<String> values = data.get(option);
        if (values == null)
            throw new OptionUsageException("option --" + option.getName() + " was not set");
        if (values.size() == 0)
            return null;
        return values.get(0);
    }

    public List<String> getValuesFor(Option option) {
        return data.get(option);
    }

    public boolean hasArgument(Option option) {
        List<String> values = data.get(option);
        if (values == null)
            throw new OptionUsageException("option --" + option.getName() + " was not set");
        return values.size() > 0;
    }

    public String getNonOptionArg(int index) {
        if (index >= nonoption.size()) {
            return null;
        } else {
            return nonoption.get(index);
        }
    }

    public String[] getNonOptionArgs() {
        String[] ret = new String[nonoption.size()];
        for (int i = 0; i < nonoption.size(); i++)
            ret[i] = nonoption.get(i);
        return ret;
    }

    public String getNonOptionArgOrError(int index) {
        if (index >= nonoption.size()) {
            throw new OptionUsageException("required argument #" + (index + 1) + " not given");
        } else {
            return nonoption.get(index);
        }
    }
}
