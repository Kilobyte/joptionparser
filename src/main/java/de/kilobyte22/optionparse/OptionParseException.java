package de.kilobyte22.optionparse;

public class OptionParseException extends RuntimeException {
    public OptionParseException(String s) {
        super(s);
    }
}
