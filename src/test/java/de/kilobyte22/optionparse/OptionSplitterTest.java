package de.kilobyte22.optionparse;

import junit.framework.TestCase;

public class OptionSplitterTest extends TestCase {
    public void testSplitArgs() throws Exception {
        String[] res = OptionParser.splitArgs("a b c");
        assertEquals("res[0] was not a", "a", res[0]);
        assertEquals("res[1] was not b", "b", res[1]);
        assertEquals("res[2] was not c", "c", res[2]);

        res = OptionParser.splitArgs("\"arg with spaces\" anotherarg");
        assertEquals("arg with spaces", res[0]);
        assertEquals("anotherarg", res[1]);

        res = OptionParser.splitArgs("arg\\ with\\ spaces anotherarg");
        assertEquals("arg with spaces", res[0]);
        assertEquals("anotherarg", res[1]);
    }


}
