package de.kilobyte22.optionparse;

import junit.framework.TestCase;

public class OptionParserTest extends TestCase {
    public void testParse() throws Exception {
        OptionParser p = new OptionParser();
        OptionSet res = p.parse(new String[]{"a", "b", "c"});
        assertEquals("Argument 1 was not a", res.getNonOptionArg(0), "a");
        assertEquals("Argument 2 was not b", res.getNonOptionArg(1), "b");
        assertEquals("Argument 3 was not c", res.getNonOptionArg(2), "c");
        assertTrue("Argument 4 was not null", res.getNonOptionArg(3) == null);
    }

    public void testFlags() throws Exception {
        OptionParser p = new OptionParser();
        Option test = p.addOption("test");
        OptionSet res = p.parse(new String[]{"arg1", "--test", "arg2", "arg3"});
        assertTrue("--test flag was not detected", res.isSet(test));
        assertFalse("--test somehow had an argument detected", res.hasArgument(test));
        assertEquals("Argument 1 was not arg1", res.getNonOptionArg(0), "arg1");
        assertEquals("Argument 2 was not arg2", res.getNonOptionArg(1), "arg2");
        assertEquals("Argument 3 was not arg3", res.getNonOptionArg(2), "arg3");

        res = p.parse(new String[]{"--test"});
        assertTrue("--test was not detected as last param", res.isSet(test));
    }

    public void testArgumentFlags() throws Exception {
        OptionParser p = new OptionParser();
        Option test = p.addOption("test").withArgument();
        OptionSet res = p.parse(new String[]{"arg1", "--test", "arg2", "arg3"});
        assertTrue("--test flag was not detected", res.isSet(test));
        assertTrue("--test somehow had no argument detected", res.hasArgument(test));
        assertEquals("--test argument was not arg2", res.getValueFor(test), "arg2");
        assertEquals("Argument 1 was not arg1", res.getNonOptionArg(0), "arg1");
        assertEquals("Argument 3 was not arg3", res.getNonOptionArg(1), "arg3");

        try {
            p.parse(new String[]{"--test"});
            fail("Did not complain about missing argument");
        } catch (OptionParseException e) {

        }
    }
}
